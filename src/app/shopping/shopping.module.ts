import {NgModule} from '@angular/core';
import {ShoppingCartSummaryComponent} from "./components/shopping-cart-summary/shopping-cart-summary.component";
import {OrderSuccessComponent} from "./components/order-success/order-success.component";
import {ShippingFormComponent} from "./components/shipping-form/shipping-form.component";
import {CheckOutComponent} from "./components/check-out/check-out.component";
import {ProductsComponent} from "./components/products/products.component";
import {ShoppingCartComponent} from "./components/shopping-cart/shopping-cart.component";
import {ProductFilterComponent} from "./components/products/product-filter/product-filter.component";
import {MyOrdersComponent} from "./components/my-orders/my-orders.component";
import {RouterModule} from "@angular/router";
import {AuthGuard} from "shared/services/auth-guard.service";
import {SharedModule} from "shared/shared.module";
import { BuyNowComponent } from './components/payments/buy-now/buy-now.component';
import { StripePipe } from './components/payments/stripe.pipe';
import {PaymentsComponent} from "./components/payments/make-payment/payments.component";
import { SendEmailComponent } from './components/send-email/send-email.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {path: 'products', component: ProductsComponent},
      {path: 'shopping-cart', component: ShoppingCartComponent},
      {path: 'check-out', component: CheckOutComponent, canActivate: [AuthGuard]},
      {path: 'order-success/:id', component: OrderSuccessComponent, canActivate: [AuthGuard]},
      {path: 'payments/:id', component: PaymentsComponent, canActivate: [AuthGuard]},
      {path: 'my/orders', component: MyOrdersComponent, canActivate: [AuthGuard]},
    ])
  ],
  declarations: [
    ProductsComponent,
    ShoppingCartComponent,
    CheckOutComponent,
    OrderSuccessComponent,
    MyOrdersComponent,
    ProductFilterComponent,
    ShoppingCartSummaryComponent,
    ShippingFormComponent,
    PaymentsComponent,
    BuyNowComponent,
    StripePipe,
    SendEmailComponent,
  ]
})
export class ShoppingModule {
}
