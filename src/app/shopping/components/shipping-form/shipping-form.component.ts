import {Component, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {Order} from "../../../shared/models/order";
import {Subscription} from "rxjs/Subscription";
import {OrderService} from "../../../shared/services/order.service";
import {Router} from "@angular/router";
import {AuthService} from "../../../shared/services/auth.service";
import {ShoppingCart} from "../../../shared/models/shopping-cart";
import {environment} from "../../../../environments/environment";
import {PaymentService} from "shared/services/payments.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.css']
})
export class ShippingFormComponent implements OnInit, OnDestroy {
  @Input('cart') cart: ShoppingCart;
  shipping = {};
  userSubscription: Subscription;
  userId: string;

  handler: any;
  amount: number = 500;
  endpoint = 'https://us-central1-oshop-fcda6.cloudfunctions.net/httpEmail';

  constructor(private router: Router,
              private authService: AuthService,
              private orderService: OrderService,
              private paymentSvc: PaymentService,
              private http: HttpClient) {
  }

  ngOnInit() {
    this.userSubscription = this.authService.user$.subscribe(user => this.userId = user.uid);
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: '/assets/cooking.jpg',
      locale: 'auto',
      token: async token => {
        this.paymentSvc.processPayment(token, this.amount);
        let order = new Order(this.userId, this.shipping, this.cart);
        let result = await this.orderService.placeOrder(order);
        this.router.navigate(['/order-success', result.key]);
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  sendEmail() {
    const data = {
      toEmail: 'y.mangala39@gmail.com',
      toName: 'Maurice Mangala'
    };

    this.http.post(this.endpoint, data).subscribe();
  }

  async handlePayment() {
    this.handler.open({
      name: 'Cooking Shop',
      excerpt: 'Deposit Funds to Account',
//      amount: this.amount
    });
  }

  @HostListener('window:popstate')
  async onPopstate() {
    this.handler.close();
  }
}
