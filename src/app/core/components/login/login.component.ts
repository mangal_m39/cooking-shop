import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';
import {Http} from "@angular/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email: string;
  password: string;

  constructor(private auth: AuthService, private http: Http) {
  }

  loginWithGoogle() {
    this.auth.loginWithGoogle();
  }

  signUp() {
    this.auth.signUp(this.email, this.password);
    this.email = this.password = '';
  }
}
