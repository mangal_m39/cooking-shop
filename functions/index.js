// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const SENDGRID_API_KEY = functions.config().sendgrid.key;

const sgMail =  require('@sendgrid/mail');
sgMail.setApiKey(SENDGRID_API_KEY);

exports.firestoreEmail = functions.firestore
  .document('users/{userId}/followers/{followerId}')
  .onCreate(event => {
    const userId = event.params.userId;
    const db = admin.firestore();

    return db.collection('users')
      .get()
    .then(doc => {
      const user = doc.data();
      const msg = {
        to: user.email,
        from: 'y.mangala39@gmail.com',
        subject: 'New follower',

        templateId:'65838fd8-1335-46f9-8508-59dc099f68fe',
        substitutionsWrappers: ['{{','}}'],
        substitutions:  {
          name: user.displayName
        }

      };
      return sgMail.send(msg);
}).then(() => console.log('email sent!'))
.catch(err => console.log(err))
});

const sendgrid = require('sendgrid');
const client = sendgrid("SG.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");

function parseBody(body) {
  var helper = sendgrid.mail;
  var fromEmail = new helper.Email(body.from);
  var toEmail = new helper.Email(body.to);
  var subject = body.subject;
  var content = new helper.Content('text/html', body.content);
  var mail = new helper.Mail(fromEmail, subject, toEmail, content);
  return  mail.toJSON();
}

exports.httpEmail = functions.https.onRequest((req, res) => {
  return Promise.resolve()
    .then(() => {
    if (req.method !== 'POST') {
  const error = new Error('Only POST requests are accepted');
  error.code = 405;
  throw error;
}

const request = client.emptyRequest({
  method: 'POST',
  path: '/v3/mail/send',
  body: parseBody(req.body)
});

return client.API(request);
})
.then((response) => {
  if (response.body) {
  res.send(response.body);
} else {
  res.end();
}
})
.catch((err) => {
  console.error(err);
return Promise.reject(err);
});


});

const stripe = require('stripe')(functions.config().stripe.testkey);



exports.stripeCharge = functions.database
  .ref('/payments/{userId}/{paymentId}')
  .onWrite(event => {



  const payment = event.data.val();
const userId = event.params.userId;
const paymentId = event.params.paymentId;


// checks if payment exists or if it has already been charged
if (!payment || payment.charge) return;

return admin.database()
  .ref(`/users/${userId}`)
  .once('value')
  .then(snapshot => {
  return snapshot.val();
})
.then(customer => {

  const amount = payment.amount;
const idempotency_key = paymentId;  // prevent duplicate charges
const source = payment.token.id;
const currency = 'usd';
const charge = {amount, currency, source};


return stripe.charges.create(charge, { idempotency_key });

})
.then(charge => {
  admin.database()
  .ref(`/payments/${userId}/${paymentId}/charge`)
  .set(charge)
})


});
