import {Component, HostListener, Input, OnInit} from '@angular/core';
import {PaymentService} from "shared/services/payments.service";
import {environment} from "../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Http} from "@angular/http";
import {ShoppingCart} from "shared/models/shopping-cart";

@Component({
  selector: 'make-payment',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent {

  constructor() {
  }
}
